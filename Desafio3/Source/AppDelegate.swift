//
//  AppDelegate.swift
//  Desafio3
//
//  Created by André Felipe de Sousa Almeida - AAD on 30/09/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let rootViewController = UIViewController()
        rootViewController.view.backgroundColor = .systemPink
        let navigagtionController = UINavigationController(rootViewController: rootViewController)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigagtionController
        window?.makeKeyAndVisible()
        return true
    }
}
